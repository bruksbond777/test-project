import http from 'k6/http';
import { sleep, check } from 'k6';

export const options = {
  stages: [
    { duration: '10s', target: 10 }, // below normal load
    { duration: '10s', target: 20 }, // spike to 1400 users
    { duration: '10s', target: 200 }, // spike to 1400 users
  ],
};

export default function () {

  let uniqid = Date.now();

  const resp = http.get(`https://devwp.givestart.org/wp-content/plugins/vd-givestart/classes/libs/ViewerCounter/ViewerCounter.php?id=361774`);

  sleep(1);
}
